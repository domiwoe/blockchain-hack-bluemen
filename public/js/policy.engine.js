var policyEngine = {};

policyEngine.calculate = function (inputVars, policy, callback) {
    function fullfillRule(rule) {
        var fullfillment = {};

        if (typeof rule ==="undefined" || rule === null) {
            return {};
        }

        var effects = rule["then"];

        if (typeof effects ==="undefined" || effects === null) {
            return {};
        }

        for (var i = 0; i < effects.length; i++) {
            var effect = effects[i];

            if (typeof effect ==="undefined" || effect === null) {
                return {};
            }

            var target = effect["target"];

            if (typeof target ==="undefined" || target === null) {
                return {};
            }

            var action = effect["action"];

            if (typeof action ==="undefined" || action === null) {
                return {};
            }

            var value = effect["value"];

            if (typeof value ==="undefined" || value === null) {
                return {};
            }

            if (action === "subtract") {
                value = value * -1;
            }

            if (typeof fullfillment[target] === "undefined" || fullfillment[target] === null) {
                fullfillment[target] = 0.0;
            }

            fullfillment[target] += value;
        }

        return fullfillment;
    }

    $.get("./js/" + policy, function(policyData) {
        if (typeof policyData === "undefined" || policyData === null) {
            console.log("policy not found");
            callback({});
            return;
        }

        var outputVars = {};

        if (typeof inputVars === "undefined" || inputVars === null) {
            callback({"error": "inputVars undefined or null"});
            return;
        }

        for (var ruleSetKey in policyData) {
            var ruleSet = policyData[ruleSetKey];

            if (typeof ruleSet === "undefined" || ruleSet === null) {
                callback({"error": "ruleSet undefined or null"});
                return;
            }

            for (var i = 0; i < ruleSet.length; i++) {
                var rule = ruleSet[i];

                if (typeof rule === "undefined" || rule === null) {
                    callback({"error": "rule undefined or null"});
                    return;
                }

                var requirements = rule["when"];

                if (typeof requirements === "undefined" || requirements === null) {
                    callback({"error": "requirements undefined or null"});
                    return;
                }

                for (var w = 0; w < requirements.length; w++) {
                    var requirement = requirements[w];

                    var variable = requirement["variable"];
                    var input = inputVars[variable];

                    if (typeof input === "undefined" || input === null) {
                        continue;
                    }

                    var comparator = requirement["comparator"];

                    if (typeof comparator === "undefined" || comparator === null) {
                        callback({"error": "comparator undefined or null"});
                        return;
                    }

                    var requiredValue = requirement["value"];

                    if (typeof requiredValue === "undefined" || requiredValue === null) {
                        callback({"error": "requiredValue undefined or null"});
                        return;
                    }

                    var fullfillment = {};

                    if (comparator.trim().toLowerCase() === "equals") {
                        if (input == requiredValue) {
                            fullfillment = fullfillRule(rule);
                        }
                    }

                    for (var ffKey in fullfillment) {
                        if (typeof outputVars[ffKey] === "undefined" || outputVars[ffKey] === null) {
                            outputVars[ffKey] = fullfillment[ffKey];
                        } else {
                            outputVars[ffKey] += fullfillment[ffKey];
                        }
                    }
                }
            }
        }

        callback(outputVars);
    });
};

window.policyEngine = policyEngine;