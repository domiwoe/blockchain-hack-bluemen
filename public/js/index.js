window.addEventListener('load', function () {

    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        // Use Mist/MetaMask's provider
        web3 = new Web3(web3.currentProvider);
    } else {
        console.log('No web3? You should consider trying MetaMask!')
        // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
        web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
    }

    var account = web3.eth.accounts[0];

    web3.eth.getBalance(account, function (error, result) {
        if (!error) {
            console.log(result);
            $("#etherAmount").html(web3.fromWei(result.toString()) + " ETH");
        } else {
            console.error(error);
        }
    });

    web3.eth.getBlockNumber(function (error, result) {
        if (!error) {
            console.log(result);
            $("#block").html(result);
        } else {
            console.error(error);
        }
    });

    web3.version.getNetwork(function (error, result) {
        if (!error) {
            console.log(result);
            if (result == 1) {
                result = "Main Ethereum Network";
            } else if ( result == 2 ) {
                result = "Morden Test Network";
            }

            $("#networkName").html(result);
        } else {
            console.error(error);
        }
    });

    web3.version.getNode(function (error, result) {
        if (!error) {
            console.log(result);
            $("#node").html(result);
        } else {
            console.error(error);
        }
    });


    $("#accountNo").html(account);

    var accountInterval = setInterval(function () {
        if (web3.eth.accounts[0] !== account) {
            account = web3.eth.accounts[0];

            $("#accountNo").html(account);

        }
    }, 100);


}); 