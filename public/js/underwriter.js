$(document).ready(function () {
    $.fn.form.settings.rules.validAccount = function (value) {
        return web3.isAddress(value);
    };

    $('form#offer-form.ui.form').form({
        fields: {
            insured: {
                identifier: 'insured',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter the address of the insured person'
                    },
                    {
                        type: 'validAccount',
                        prompt: 'Please enter a valid account address'
                    }
                ]
            },
            insuranceSum: {
                identifier: 'insurance-sum',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter the insurance sum'
                    }
                ]
            },
            premium: {
                identifier: 'premium',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Please enter the premium'
                    }
                ]
            }
        }
    });
    $("#error").hide();
});

function showErrorMsg(msg) {
    $("#error").show();
    $("#error_msg").html(msg);
}

function updateInterface() {
    web3.version.getNetwork(function (error, result) {
        var networkName = "Unknown";

        switch (parseInt(result)) {
            case 1:
                networkName = "Main Network";
                break;
            case 2:
                networkName = "Morden Testnet";
                break;
        }

        $("span#meta_network").html(networkName);
    });

    $("span#meta_account").html(web3.eth.accounts[0]);
}


function callContract() {
    
}

window.addEventListener('load', function () {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        // Use Mist/MetaMask's provider
        web3 = new Web3(web3.currentProvider);

        var account = web3.eth.accounts[0];

        if (typeof account !== "undefined" && account !== null) {
            updateInterface();
        }

        var accountInterval = setInterval(function () {
            if (web3.eth.accounts[0] !== account) {
                account = web3.eth.accounts[0];
                updateInterface();
            }
        }, 100);
    } else {
        console.log('No web3? You should consider trying MetaMask!');
    }
});