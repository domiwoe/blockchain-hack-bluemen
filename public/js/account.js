function updateInterface() {
    // Variables...
    var policyEngine = window.policyEngine;
    var offerSent = false;
    var account = web3.eth.accounts[0];
    var offerJson = {};
    var premium = 0;

    updateEthereumDataToScreen(account);

    var accountInterval = setInterval(function () {
        if (web3.eth.accounts[0] !== account) {
            account = web3.eth.accounts[0];
            $("#accountNo").html(account);
        }
    }, 100);

    // Register semantic ui dropdown
    $('.ui.dropdown')
        .dropdown();

    // Check if form complete
    $('input, select, textarea', $('#premiumform')).change(function () {

        var bfirsname = $('#firstname').val() !== '';
        var blastname = $('#lastname').val() !== '';
        var bemail = $('#email').val() !== '';
        var bsmokers = $('#smoker').val() !== '';
        var b_age = $('#age').val() !== '';
        var bamount = $('#amount').val() !== '';
        var bbeneficiary = $('#beneficiary').val() !== '';

        if (bfirsname && blastname && bemail && b_age && bamount && bbeneficiary && bsmokers) {
            // map fields.
            $("#policyfname").html($('#firstname').val());
            $("#policyname").html($('#lastname').val());
            $("#policyage").html($('#age').val());
            $("#policysmoker").html($("#smoker option:selected").text());
            $("#policyemail").html($('#email').val());
            $("#policyinsuredvalue").html(parseFloat($('#amount').val(), 10).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString() + " CHF");
            $("#policyname").html($('#lastname').val());

            // enrich object.
            offerJson = {
                walletId: $('#walletid').val(),
                firstname: $('#firstname').val(),
                lastname: $('#lastname').val(),
                age: $("#age").val(),
                smoker: $("#smoker option:selected").text(),
                email: $("#email").val(),
                insuredValue: parseInt($("#amount").val()),
                beneficiary: $('#beneficiary').val()
            };
            // calculate premium.
            calculatePremium();

            // Show policy.
            $("#policyPage").removeClass("hidden");
        } else {
            $("#policyPage").addClass("hidden");
        }


    });

    // send offer to underwriter.
    $("#sendoffer").click(function () {

        $.post("http://localhost:3030/" + $("select#policy").val(), offerJson, function (data, status) {
            console.log("Sent!");
        }, 'json')
            .done(function (data) {
                $("#wait_for_event").removeClass("hidden");
                $("#send_offer_buttons").addClass("hidden");
                $("#errorBox").addClass("hidden");
                $("#errorMsg").html('');
                registerApprovalEvent();
            })
            .fail(function (e) {
                $("#errorBox").removeClass("hidden");
                $("#errorMsg").html(e.responseText);
                console.log(e);
                $('#send_offer_buttons').addClass("hidden");

            })

        offerSent = true;
        console.log(offerJson);
    });


    // pay for the polict .
    $("#pay").click(function () {
        console.log("Client payed.");

        pool.payPremium.sendTransaction({
            value: web3.toWei(web3.fromWei(parseInt(offerJson.premium))),
            gas: 300000
        }, function (err, res) {
            console.log(err);
            if (err) {
                console.log(err);
                $("#errorBox").removeClass("hidden");
                $("#errorMsg").html(err.responseText);
            } else {
                console.log(res);
                $("#errorBox").addClass("hidden");
                $("#completeAccount").addClass("hidden");
                $("#pay_decline_buttons").addClass("hidden");
                $("#successBox").removeClass("hidden");
                $("#successMsg").html(res);
            }
        })
    });


    function calculatePremium() {
        var ruleEngine = $("select#policy").val();
        var engineInput = {
            smoker: offerJson.smoker,
            age: offerJson.age
        };
        policyEngine.calculate(engineInput, ruleEngine, function (engineOutput) {
            //Premium = insurable amount*incidence rateapplicable to age*risk surcharges
            var riskSurchargeengine = engineOutput.risk_surcharge + 1;

            offerJson.premium = parseInt(offerJson.insuredValue) *
                parseFloat(engineOutput.mortality_rate) *
                parseFloat(riskSurchargeengine);
            $("span#premium").html(Math.round(parseFloat(offerJson.premium),2));
        });
    }
}


function updateEthereumDataToScreen(account) {
    web3.eth.getBalance(account, function (error, result) {
        if (!error) {
            console.log(result);
            $("#etherAmount").html(web3.fromWei(result.toString()) + " ETH");
        } else {
            console.error(error);
        }
    });

    web3.eth.getBlockNumber(function (error, result) {
        if (!error) {
            console.log(result);
            $("#block").html(result);
        } else {
            console.error(error);
        }
    });

    web3.version.getNetwork(function (error, result) {
        if (!error) {
            console.log(result);
            if (result == 1) {
                result = "Main Ethereum Network";
            } else if (result == 2) {
                result = "Morden Test Network";
            }

            $("#networkName").html(result);
        } else {
            console.error(error);
        }
    });

    web3.version.getNode(function (error, result) {
        if (!error) {
            console.log(result);
            $("#node").html(result);
        } else {
            console.error(error);
        }
    });

    $("#walletid").val(account);
}

var pool = {};

function registerApprovalEvent() {
    $.get("http://localhost:3030/registrar/contract/" + $("select#policy").val(), function (result) {
        console.log(result);
        console.log("Registrar there.");

        var contractAddress = result["contract"];
        var contractABI = result["contractABI"];

        console.log("contractAddress: " + contractAddress);
        console.log("contractABI: " + JSON.stringify(contractABI));

        var contractObject = web3.eth.contract(contractABI);
        pool = contractObject.at(contractAddress);

        var event = pool.Approved();

        // watch for changes
        event.watch(function (error, result) {
            if (!error) {
                console.log("Event received!");
                console.log(result);
                enableApprovalButton()
            } else {
                console.log("Event error");
                console.log(result);
            }
        });

    });
}


function enableApprovalButton() {
    $('#wait_for_event').addClass("hidden");
    $('#pay_decline_buttons').removeClass("hidden");
}

window.addEventListener('load', function () {
    // Checking if Web3 has been injected by the browser (Mist/MetaMask)
    if (typeof web3 !== 'undefined') {
        // Use Mist/MetaMask's provider
        web3 = new Web3(web3.currentProvider);

        var account = web3.eth.accounts[0];

        if (typeof account !== "undefined" && account !== null) {
            updateInterface();
        }

        var accountInterval = setInterval(function () {
            if (web3.eth.accounts[0] !== account) {
                account = web3.eth.accounts[0];
                updateInterface();
            }
        }, 100);
    } else {
        console.log('No web3? You should consider trying MetaMask!');
    }
});
