contract pool {
    
    address reinsurer;
    address underwriter;
    
    uint reinsuranceFee;

    uint insuredID = 0;
    uint claimID = 0;
    
    uint totalClaims = 0;
    uint totalProfit = 0;
    uint totalPremium = 0;
    uint totalReinsuranceFee = 0;
    
    uint negativeBalance = 0;
    
    bool blockWithdraw = true;
    
    bytes32 policy;
    
    event Reinsurance(
        uint amount
    );
    
    
    // Notify requester that she is approved and may pay the premium
    event Approved(
        address requester
    );
    
    // Stakeholders can withdraw their balance
    event WithdrawOpen();

    
    modifier underwritingApproved() {
        
        if (insurences[msg.sender].paymentPending != true) throw; 
        _;
    }
    
    modifier onlyUnderwriter() {
        if (msg.sender != underwriter) throw;
        _;
    }
    
     modifier onlyReinsurer() {
        if (msg.sender != reinsurer) throw;
        _;
    }
    
    // check validity
    struct Insurance {
        address beneficiary;
        uint premium;
        uint insurableAmount;
        bool paymentPending;
    }
    
    
    
    mapping(address => Insurance) public insurences;
    
    mapping(address => bool) public approved;
    
    mapping(uint => address) public insureds;
    
    mapping(uint => address) public claims;
    
    mapping(address => uint) public balances;
    
    
    function Pool(address _reinsurer, address _underwriter, uint _reinsuranceFee, bytes32 _policy) {
        
        reinsurer = _reinsurer; 
        underwriter = _underwriter;
        
        reinsuranceFee = _reinsuranceFee;
        
        policy = _policy; // hash of serialized json representing the policy
        
    }

    // Underwriter approves a new potential pool member
    // ATTENTION: Removed onlyUnderwriter temporarily 
    function approveUnderwriting(address insured, address beneficiary, uint premium, uint insurableAmount) {
        
        // ToDo: Check if already approved
        
        insurences[insured] = Insurance(beneficiary,premium,insurableAmount,true);
        Approved(insured);
        
    }
    
    function addClaim(address insured) onlyUnderwriter {
        
        // ToDo: Check if already added
        
        claims[claimID] = insured;
        claimID++;
        
    }
    
    
    // Settle everything at the end of the period
    function doSettlement() {
        
        for(uint i = 0; i < claimID; i++) {
            
            balances[insurences[claims[i]].beneficiary] = insurences[claims[i]].insurableAmount;
            totalClaims = totalClaims + insurences[claims[i]].insurableAmount;
   
        }
        
        totalPremium = this.balance;
        
        totalReinsuranceFee = totalPremium * reinsuranceFee/100;
        
        
        // Claims are more than balance of contract => reinsurer has to fill up
        if (totalClaims > this.balance) {
            negativeBalance = totalClaims-this.balance;
            Reinsurance(negativeBalance);
        // Claims are lower than balance but nothing left for profit
        } else if (totalClaims > this.balance - totalReinsuranceFee ) {
            
            balances[reinsurer] = this.balance - totalClaims;
            
            blockWithdraw = false;
            WithdrawOpen();
            
        } else {
            
            balances[reinsurer] = totalReinsuranceFee;
            
            totalProfit = totalPremium - totalReinsuranceFee - totalClaims;
            
            for(uint j = 0; j < insuredID; j++) {
                
                address insured = insureds[j];
            
                balances[insured] = insurences[insured].premium * totalProfit/totalPremium;
    
            }
            
            blockWithdraw = false;
            WithdrawOpen();
            
        }
        
    }
    
    function withdrawBalance() {
        
        if (!blockWithdraw) throw;
        
        uint amount = balances[msg.sender];
        
        balances[msg.sender] = 0;
        
        if (!msg.sender.send(amount)) throw;
        
    }
    
    function getBalance() constant returns (uint) {
        
        return balances[msg.sender];
    }
    
    function getTotalPremium() constant returns (uint) {
        
        return this.balance;
    
    }
    
    
    function addInsured(address insured) {
        
        insureds[insuredID] = insured;
        
        insuredID++;
        
        insurences[insured].paymentPending = false;
    
    }
    
    // Reinsurer has to cover claims if pool is insolvent
    function coverClaims() onlyReinsurer {
        
        if(msg.value < negativeBalance) throw;
        
        blockWithdraw = false;
        
        WithdrawOpen();
        
    }
    
    
    // ATTENTION: Removed underwritingApproved temporarily 
    function payPremium() {
        
        // Throw if value is below premium
        //if (msg.value < insurences[msg.sender].premium) throw;
        
        addInsured(msg.sender);
        
        // Be nice and send back amount paid above premium
        //if (msg.value > insurences[msg.sender].premium) {
        //    msg.sender.send(msg.value - insurences[msg.sender].premium);
        //}
        
    }
}