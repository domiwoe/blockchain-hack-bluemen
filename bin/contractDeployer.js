/*
// Deploys a contract to Morden Testnet
*/

var sha256 = require('js-sha256').sha256;
var w3 = require('web3');
var fs = require('fs');
var redis = require("redis"),
    redisClient = redis.createClient({
        "host": "thedev.de",
        "port": 6379
    });

require.extensions['.sol'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

var gethUrl = 'http://localhost:8545';

var contractSource = require("../contracts/pool.sol");
var contractName = 'pool';
var policyObj = require('../public/js/policy_rules.json');


var fromAddress = '0x961230418dfd410c9b6bad54123cbf0ba27313c8';
var _underwriter = '0x3c4c751fc08d110907384562f0c5f645b203dda6';
var _reinsurer = '0x2fb02443e29b2fdddfd326d1ab9c7b824ebc1913';
var _reinsuranceFee = 10;

var web3 = new w3(new w3.providers.HttpProvider(gethUrl));

var policyString = JSON.stringify(policyObj);
var _policy = sha256(policyString);


function deploy(contract, cb) {
  //var code = contract[contractName].code;
  //var abi = contract[contractName].abiDefinition;

  //var contractObj = web3.eth.contract(abi);

  var poolContract = web3.eth.contract([{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"insurences","outputs":[{"name":"beneficiary","type":"address"},{"name":"premium","type":"uint256"},{"name":"insurableAmount","type":"uint256"},{"name":"paymentPending","type":"bool"}],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getBalance","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"insured","type":"address"},{"name":"beneficiary","type":"address"},{"name":"premium","type":"uint256"},{"name":"insurableAmount","type":"uint256"}],"name":"approveUnderwriting","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"balances","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"payPremium","outputs":[],"payable":true,"type":"function"},{"constant":false,"inputs":[],"name":"doSettlement","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"withdrawBalance","outputs":[],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"insured","type":"address"}],"name":"addClaim","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[],"name":"getTotalPremium","outputs":[{"name":"","type":"uint256"}],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"insureds","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[],"name":"coverClaims","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"uint256"}],"name":"claims","outputs":[{"name":"","type":"address"}],"payable":false,"type":"function"},{"constant":false,"inputs":[{"name":"insured","type":"address"}],"name":"addInsured","outputs":[],"payable":false,"type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"approved","outputs":[{"name":"","type":"bool"}],"payable":false,"type":"function"},{"inputs":[{"name":"_reinsurer","type":"address"},{"name":"_underwriter","type":"address"},{"name":"_reinsuranceFee","type":"uint256"},{"name":"_policy","type":"bytes32"}],"payable":false,"type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"amount","type":"uint256"}],"name":"Reinsurance","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"requester","type":"address"}],"name":"Approved","type":"event"},{"anonymous":false,"inputs":[],"name":"WithdrawOpen","type":"event"}]);
  var pool = poolContract.new(
     _reinsurer,
     _underwriter,
     _reinsuranceFee,
     _policy,
     {
       from: fromAddress,
       data: '0x606060405260006003556000600455600060055560006006556000600755600060085560006009556001600a60006101000a81548160ff02191690837f0100000000000000000000000000000000000000000000000000000000000000908102040217905550346100005760405160808061107b833981016040528080519060200190919080519060200190919080519060200190919080519060200190919050505b83600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c0100000000000000000000000090810204021790555082600160006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c010000000000000000000000009081020402179055508160028190555080600b819055505b505050505b610f3d8061013e6000396000f3606060405236156100c8576000357c01000000000000000000000000000000000000000000000000000000009004806305258a13146100cd57806312065fe01461012b578063168e4b5b1461014e57806327e235e31461018657806329c08ba2146101b7578063427f49dc146101c15780635fd8c710146101d057806369f46779146101df57806373f16751146101fc57806399cd3e201461021f5780639d499f9214610266578063a888c2cd14610275578063ca2d3112146102bc578063d8b964e6146102d9575b610000565b34610000576100e8600480803590602001909190505061030c565b604051808573ffffffffffffffffffffffffffffffffffffffff168152602001848152602001838152602001821515815260200194505050505060405180910390f35b3461000057610138610369565b6040518082815260200191505060405180910390f35b3461000057610184600480803590602001909190803590602001909190803590602001909190803590602001909190505061039b565b005b34610000576101a16004808035906020019091905050610538565b6040518082815260200191505060405180910390f35b6101bf610550565b005b34610000576101ce610688565b005b34610000576101dd610b6b565b005b34610000576101fa6004808035906020019091905050610c2a565b005b3461000057610209610ced565b6040518082815260200191505060405180910390f35b346100005761023a6004808035906020019091905050610d0d565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b3461000057610273610d40565b005b34610000576102906004808035906020019091905050610e1c565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34610000576102d76004808035906020019091905050610e4f565b005b34610000576102f46004808035906020019091905050610f1d565b60405180821515815260200191505060405180910390f35b600c6020528060005260406000206000915090508060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060010154908060020154908060030160009054906101000a900460ff16905084565b6000601060003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205490505b90565b600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156103f757610000565b6080604051908101604052808481526020018381526020018281526020016001815260200150600c60008673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c01000000000000000000000000908102040217905550602082015181600101556040820151816002015560608201518160030160006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055509050507f5d91bd0cecc45fef102af61de92c5462fadc884a5ce9d21c15e8a85198f2349e84604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a15b5b50505050565b60106020528060005260406000206000915090505481565b60011515600c60003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060030160009054906101000a900460ff16151514151561059c57610000565b600c60003373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600101543410156105d557610000565b6105de33610e4f565b600c60003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060010154341115610684573373ffffffffffffffffffffffffffffffffffffffff166108fc600c60003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206001015434039081150290604051809050600060405180830381858888f19350505050151561068357610000565b5b5b5b565b600060006000600092505b60045483101561082157600c6000600f600086815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206002015460106000600c6000600f600089815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550600c6000600f600086815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060020154600554016005819055505b8280600101935050610693565b3073ffffffffffffffffffffffffffffffffffffffff1631600781905550606460025460075402811561000057046008819055503073ffffffffffffffffffffffffffffffffffffffff163160055411156108d6573073ffffffffffffffffffffffffffffffffffffffff1631600554036009819055507fe53e3a2fba26ecbba77d4620330414e0b113cbf431e5472db4ed21a2337b2d476009546040518082815260200191505060405180910390a1610b65565b6008543073ffffffffffffffffffffffffffffffffffffffff16310360055411156109d8576005543073ffffffffffffffffffffffffffffffffffffffff16310360106000600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055506000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a1610b64565b60085460106000600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055506005546008546007540303600681905550600091505b600354821015610af657600e600083815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050600754600654600c60008473ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600101540281156100005704601060008373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055505b8180600101925050610a40565b6000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a15b5b5b505050565b6000600a60009054906101000a900460ff161515610b8857610000565b601060003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000205490506000601060003373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020819055503373ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051809050600060405180830381858888f193505050501515610c2657610000565b5b50565b600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610c8657610000565b80600f6000600454815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c010000000000000000000000009081020402179055506004600081548092919060010191905055505b5b50565b60003073ffffffffffffffffffffffffffffffffffffffff163190505b90565b600e6020528060005260406000206000915054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610d9c57610000565b600954341015610dab57610000565b6000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a15b5b565b600f6020528060005260406000206000915054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b80600e6000600354815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c010000000000000000000000009081020402179055506003600081548092919060010191905055506000600c60008373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060030160006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055505b50565b600d6020528060005260406000206000915054906101000a900460ff168156',
       gas: '4700000'
     }, function (e, contract){
      console.log(e, contract);
      if (typeof contract.address !== 'undefined') {
           console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
           var policy = {
               "contract": contract.address,
               "contractABI": abi,
               "policy:": policyObj
           };
           policy = JSON.stringify(policy);
           redisClient.set("policy:policy_rules.json", policy, function (err, res) {
               console.log(res);
           });
      }
   })

  // var deployed = contractObj.new(reinsurer, underwriter, fee, policyHash,
  //
  //     {
  //         from: fromAddress,
  //         data: code,
  //         gas: '4700000'
  //     }, function (e, contract) {
  //         console.log(e, contract);
  //         if (typeof contract.address !== 'undefined') {
  //             console.log('Contract minebrod! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);
  //             var policy = {
  //                 "contract": contract.address,
  //                 "contractABI": abi,
  //                 "policy:": policyObj
  //             };
  //             policy = JSON.stringify(policy);
  //             redisClient.set("policy:policy_rules.json", policy, function (err, res) {
  //                 console.log(res);
  //             });
  //
  //         }
  //     });

      //cb(null,deployed);

}


// Compile contract
// var compiled = web3.eth.compile.solidity(contractSource, function(err, res) {
//   if (err) {
//     console.log(err);
//   } else {
//
//     deploy(contract, function(err, res) {
//       console.log(res);
//     })
//
//   }
// });

deploy()
