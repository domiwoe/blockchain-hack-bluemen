var express = require('express');
var router = express.Router();

/* GET  */
router.get('/', function(req, res) {
    res.render('claim', { title: 'Insurance registration' });
});

module.exports = router;
