var express = require('express');
var router = express.Router();
var sha256 = require('js-sha256').sha256;
var w3 = require('web3');
var redis = require("redis"),
    redisClient = redis.createClient({
        "host": "thedev.de",
        "port": 6379
    });
var fs = require('fs');


require.extensions['.sol'] = function (module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

var fromAddress = '0x737dd7bcdc7e3d7bca9e138f787760e7d2f3c7b5';
var gethUrl = 'http://localhost:8545';

var web3 = new w3(new w3.providers.HttpProvider(gethUrl));



web3.personal.unlockAccount(fromAddress, '1');

var contractABI = [{
    "constant": true,
    "inputs": [{"name": "", "type": "address"}],
    "name": "insurences",
    "outputs": [{"name": "beneficiary", "type": "address"}, {
        "name": "premium",
        "type": "uint256"
    }, {"name": "insurableAmount", "type": "uint256"}, {"name": "paymentPending", "type": "bool"}],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "getBalance",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "insured", "type": "address"}, {"name": "beneficiary", "type": "address"}, {
        "name": "premium",
        "type": "uint256"
    }, {"name": "insurableAmount", "type": "uint256"}],
    "name": "approveUnderwriting",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": true,
    "inputs": [{"name": "", "type": "address"}],
    "name": "balances",
    "outputs": [{"name": "", "type": "uint256"}],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "payPremium",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "doSettlement",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "withdrawBalance",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "insured", "type": "address"}],
    "name": "addClaim",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": true,
    "inputs": [{"name": "", "type": "uint256"}],
    "name": "insureds",
    "outputs": [{"name": "", "type": "address"}],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [],
    "name": "coverClaims",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": true,
    "inputs": [{"name": "", "type": "uint256"}],
    "name": "claims",
    "outputs": [{"name": "", "type": "address"}],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "_reinsurer", "type": "address"}, {
        "name": "_underwriter",
        "type": "address"
    }, {"name": "_reinsuranceFee", "type": "uint256"}],
    "name": "Pool",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": false,
    "inputs": [{"name": "insured", "type": "address"}],
    "name": "addInsured",
    "outputs": [],
    "payable": false,
    "type": "function"
}, {
    "constant": true,
    "inputs": [{"name": "", "type": "address"}],
    "name": "approved",
    "outputs": [{"name": "", "type": "bool"}],
    "payable": false,
    "type": "function"
}, {
    "anonymous": false,
    "inputs": [{"indexed": false, "name": "amount", "type": "uint256"}],
    "name": "Reinsurance",
    "type": "event"
}, {
    "anonymous": false,
    "inputs": [{"indexed": false, "name": "requester", "type": "address"}],
    "name": "Approved",
    "type": "event"
}, {"anonymous": false, "inputs": [], "name": "WithdrawOpen", "type": "event"}];

/* GET  */
router.get('/:jsonFile', function (req, res) {

    var policyObj = require('../public/js/' + req.param("jsonFile"));
    var policyString = JSON.stringify(policyObj);
    var policyHash = sha256(policyString);

    var poolContract = web3.eth.contract(contractABI);

    //var reinsurer = '0xEdD346db97A4344E08D2Cd96aA2ce64DfC0Cd573';
    //var underwriter ='0x1b5C123141881030A4Cc8CaF0953a77a8cA485Bf';

    redisClient.get("registrar:reinsurance", function (err, result) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        var reinsurer = result.trim();

        redisClient.get("registrar:underwriter", function (err, result) {
            if (err) {
                res.status(500).send(err);
                return;
            }

            var underwriter = result.trim();
            var fee = 10;
            var pool = poolContract.new(reinsurer, underwriter, fee, policyHash,

                {
                    from: fromAddress,
                    data: '0x606060405260006003600050556000600460005055600060056000505560006006600050556000600760005055600060086000505560006009600050556001600a60006101000a81548160ff02191690837f0100000000000000000000000000000000000000000000000000000000000000908102040217905550611081806100886000396000f3606060405236156100cc576000357c01000000000000000000000000000000000000000000000000000000009004806305258a13146100d157806312065fe01461012f578063168e4b5b1461014357806327e235e31461017b57806329c08ba2146101ac578063427f49dc146101c05780635fd8c710146101d457806369f46779146101e857806399cd3e20146102055780639d499f921461024c578063a888c2cd14610260578063bdf7ad82146102a7578063ca2d3112146102d6578063d8b964e6146102f3576100cc565b610002565b34610002576100ec6004808035906020019091905050610326565b604051808573ffffffffffffffffffffffffffffffffffffffff168152602001848152602001838152602001821515815260200194505050505060405180910390f35b3461000257610141600480505061038c565b005b3461000257610179600480803590602001909190803590602001909190803590602001909190803590602001909190505061038f565b005b34610002576101966004808035906020019091905050610538565b6040518082815260200191505060405180910390f35b34610002576101be6004805050610553565b005b34610002576101d260048050506106a2565b005b34610002576101e66004805050610c11565b005b34610002576102036004808035906020019091905050610cdc565b005b34610002576102206004808035906020019091905050610da7565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b346100025761025e6004805050610ddf565b005b346100025761027b6004808035906020019091905050610ebe565b604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b34610002576102d46004808035906020019091908035906020019091908035906020019091905050610ef6565b005b34610002576102f16004808035906020019091905050610f80565b005b346100025761030e600480803590602001909190505061105c565b60405180821515815260200191505060405180910390f35b600b6000506020528060005260406000206000915090508060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16908060010160005054908060020160005054908060030160009054906101000a900460ff16905084565b5b565b600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156103eb57610002565b6080604051908101604052808481526020018381526020018281526020016001815260200150600b60005060008673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060008201518160000160006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c01000000000000000000000000908102040217905550602082015181600101600050556040820151816002016000505560608201518160030160006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055509050507f5d91bd0cecc45fef102af61de92c5462fadc884a5ce9d21c15e8a85198f2349e84604051808273ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390a15b5b50505050565b600f6000506020528060005260406000206000915090505481565b60011515600b60005060003373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060030160009054906101000a900460ff1615151415156105a557610002565b600b60005060003373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050600101600050543410156105e757610002565b6105f033610f80565b600b60005060003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000506001016000505434111561069e573373ffffffffffffffffffffffffffffffffffffffff166108fc600b60005060003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000506001016000505434039081150290604051809050600060405180830381858888f19350505050505b5b5b565b600060006000600092505b60046000505483101561086b57600b6000506000600e600050600086815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060020160005054600f6000506000600b6000506000600e600050600089815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060000160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005081905550600b6000506000600e600050600086815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060020160005054600560005054016005600050819055505b82806001019350506106ad565b3073ffffffffffffffffffffffffffffffffffffffff1631600760005081905550606460026000505460076000505402811561000257046008600050819055503073ffffffffffffffffffffffffffffffffffffffff16316005600050541115610938573073ffffffffffffffffffffffffffffffffffffffff1631600560005054036009600050819055507fe53e3a2fba26ecbba77d4620330414e0b113cbf431e5472db4ed21a2337b2d476009600050546040518082815260200191505060405180910390a1610c0b565b6008600050543073ffffffffffffffffffffffffffffffffffffffff1631036005600050541115610a49576005600050543073ffffffffffffffffffffffffffffffffffffffff163103600f6000506000600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050819055506000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a1610c0a565b600860005054600f6000506000600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050819055506005600050546008600050546007600050540303600660005081905550600091505b600360005054821015610b9c57600d6000506000600360005054815260200190815260200160002060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff169050600760005054600660005054600b60005060008473ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050600101600050540281156100025704600f60005060008373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050819055505b8180600101925050610ac6565b6000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a15b5b5b505050565b6000600a60009054906101000a900460ff161515610c2e57610002565b600f60005060003373ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000505490506000600f60005060003373ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020600050819055503373ffffffffffffffffffffffffffffffffffffffff166108fc829081150290604051809050600060405180830381858888f193505050501515610cd857610002565b5b50565b600160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610d3857610002565b80600e6000506000600460005054815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c0100000000000000000000000090810204021790555060046000818150548092919060010191905055505b5b50565b600d60005060205280600052604060002060009150909054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b600060009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff16141515610e3b57610002565b600960005054341015610e4d57610002565b6000600a60006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055507f4f3e12934bcb8d635f23ab8ffad729f55b293fd2f2502f5f5a58e577419fd0c560405180905060405180910390a15b5b565b600e60005060205280600052604060002060009150909054906101000a900473ffffffffffffffffffffffffffffffffffffffff1681565b82600060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c0100000000000000000000000090810204021790555081600160006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c01000000000000000000000000908102040217905550806002600050819055505b505050565b80600d6000506000600360005054815260200190815260200160002060006101000a81548173ffffffffffffffffffffffffffffffffffffffff02191690836c0100000000000000000000000090810204021790555060036000818150548092919060010191905055506000600b60005060008373ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060005060030160006101000a81548160ff02191690837f01000000000000000000000000000000000000000000000000000000000000009081020402179055505b50565b600c60005060205280600052604060002060009150909054906101000a900460ff168156',
                    gas: '4700000'
                }, function (e, contract) {
                    console.log(e, contract);
                    if (typeof contract.address !== 'undefined') {
                        console.log('Contract mined! address: ' + contract.address + ' transactionHash: ' + contract.transactionHash);

                        var policy = {
                            "contract": contract.address,
                            "contractABI": contractABI,
                            "policy:": policyObj
                        };
                        policy = JSON.stringify(policy);
                        redisClient.set("policy:" + req.param("jsonFile"), policy, function (err, result) {
                            res.status(201).send('policy successfully upserted');
                        });

                    }
                })
        });
    });
});

module.exports = router;
