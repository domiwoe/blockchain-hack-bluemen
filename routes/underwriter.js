var express = require('express');
var router = express.Router();
var redis = require("redis"),
    redisClient = redis.createClient({
        "host": "thedev.de",
        "port": 6379
    });

var Web3 = require('web3/index.js');
var web3 = new Web3();

var gethUrl = 'http://ec2-35-156-44-90.eu-central-1.compute.amazonaws.com:8545';
web3.setProvider(new web3.providers.HttpProvider(gethUrl));

/* GET users listing. */
router.get('/', function (req, res) {
    res.render('index', {title: 'LifeBlock+ - Underwriter'});
});

router.post('/:policyJson', function (req, response) {
    var offer = req.body;
    console.log("Data from client received: " + JSON.stringify(offer));

    redisClient.get("registrar:underwriter", function (err, underwriterAddress) {
        if (err) {
            response.status(500).send(err);
            return;
        }

        console.log("underwriterAddress: " + underwriterAddress);
        web3.eth.defaultAccount = underwriterAddress;
        web3.personal.unlockAccount(underwriterAddress, "x39f7l25");

        redisClient.get("policy:" + req.param("policyJson"), function (err, contractOptions) {
            if (err) {
                response.status(500).send(err);
                return;
            }

            contractOptions = JSON.parse(contractOptions);
            var contractAddress = contractOptions["contract"];
            var contractABI = contractOptions["contractABI"];

            console.log("contractAddress: " + contractAddress);
            console.log("contractABI: " + JSON.stringify(contractABI));

            var contractObject = web3.eth.contract(contractABI);
            var pool = contractObject.at(contractAddress);

            var insuredAddress = offer["walletId"];
            var beneficiaryAddress = offer["beneficiary"];
            var premium = parseInt(offer["premium"]);
            var insuredValue = parseInt(offer['insuredValue']);

            console.log(insuredAddress, beneficiaryAddress, premium, insuredValue);
            console.log("insuredAddress: " + web3.isAddress(insuredAddress));
            console.log("beneficiaryAddress: " + web3.isAddress(beneficiaryAddress));

            pool.approveUnderwriting.sendTransaction(insuredAddress, beneficiaryAddress, premium, insuredValue, {
                "gas": 300000
            }, function (err, approvalResult) {
                if (err) {
                    console.log(err);
                    response.status(500).send(err);
                    return;
                }

                console.log(approvalResult);
                response.status(200).jsonp({});
            });
        });
    });
});

module.exports = router;