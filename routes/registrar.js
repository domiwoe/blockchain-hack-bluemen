var express = require('express');
var router = express.Router();
var redis = require("redis"),
    redisClient = redis.createClient({
        "host": "thedev.de",
        "port": 6379
    });

router.get('/contract/:policyJson', function (req, res) {
    redisClient.get("policy:" + req.param("policyJson"), function (err, result) {
        if (err) {
            res.status(500).send(err);
            return;
        }

        result = JSON.parse(result);
        res.json(result);
    });
});

/* GET  */
router.get('/:id', function (req, res) {
    redisClient.get("registrar:" + req.param("id"), function (err, result) {
        res.json({
            "id": req.param("id"),
            "address": result
        });
    });
});

/* GET  */
router.get('/:id/:address', function (req, res) {
    redisClient.set("registrar:" + req.param("id"), req.param("address"), function (err, result) {
        res.status(201).send('address successfully upserted');
    });
});

module.exports = router;
