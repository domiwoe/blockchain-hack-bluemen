var express = require('express');
var router = express.Router();

/* GET  */
router.get('/', function(req, res) {
    res.render('manage', { title: 'LifeBlock+ - Premium Calculator' });
});

module.exports = router;
